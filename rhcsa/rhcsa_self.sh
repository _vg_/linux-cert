
## This is a pseudo-shell script, so exit immediately in case of accidential run
exit 0


## See https://gitlab.com/_vg_/linux-cert for the up-to-date file/data
#  All is tested with CentOS Linux release 7.6.1810

## This is a workbook for:
#   Red Hat Certified System Administrator (RHCSA) Complete Video Course
#   by Sander van Vugt 
#   https://learning.oreilly.com/videos/red-hat-certified/9780133965803

yum upgrade -y
yum install -y bash-completion nmap elinks lftp epel-release tmux setools-console selinux-policy-doc

## exercise2

# check/rebuild manpages cache index, for example started with ls manpage
# man -u ls
# or just use
mandb

# add user Lisa,
#   Create a group with the same name as the user, add the user to that group.
adduser -m -d /home/lisa -s /bin/bash -U lisa


## lesson3

#
updatedb && locate ls
whereis -l sslpasswd


## exercise4

# print 5th line of /etc/passwd
sed -n 5p /etc/passwd
# print 3th to 5th lines
sed -n 3,5p /etc/passwd
# print 3th and 5th lines
sed -n 3p\;5p /etc/passwd

# #5
egrep -RI '^.{3}$' /etc/*


## exercise5

# passwordless ssh 
#   once authorized, make auth to the user@host w/o password anymore
ssh-keygen
ssh-copy-id user@remotehost


## lesson6

# user/group mgmt
useradd lisa 
groupadd mygroup
usermod -aG mygroup lisa

# set Account expires to never
# never == -1
usermod -e -1 lisa

# generic defaults for users/password
/etc/login.defs

# useradd defaults, like other ones
/etc/default/useradd

# $HOME dir content skeleton
/etc/skel

# check psw properties
chage -l user

## exercise6
...
# set password exiration in 90 days
passwd -x 90 lisa


## lesson7

# FreeIPA
# FreeIPA setup for host centos7.localdomain[192.168.0.8]
# Note https://learning.oreilly.com/library/view/practical-lpic-3-300/9781484244739/html/476407_1_En_23_Chapter.xhtml
#   from Practical LPIC-3 300: Prepare for the Highest Level Professional Linux Certification 
nmtui # set static IPv4=192.168.0.8/24, hostname=centos7.localdomain
echo 192.168.0.8 centos7.localdomain >> /etc/hosts
yum install ipa-server ipa-server-dns
ipa-servr-install
# like described in the next steps once the setup is complete,
# open ports that are in use:
for port in 53 80 88 389 443 464 636; do firewall-cmd --permanent --add-port=${port}/tcp; done
for port in 53 80 123 464; do firewall-cmd --permanent --add-port=${port}/udp; done
firewall-cmd --reload
# obtain a kerberos ticket that will allow you to use the IPA tools
# (e.g., ipa user-add) and the web user interface
# btw, `kinit' et al are come from `krb5-workstation` package
kinit admin
klist
# go to https://centos7.localdomain or https://centos7.localdomain/ipa/ui
# and login as admin with password that was set up during
# ipa-servr-install dialog set up the right "Default e-mail domain" in
# the "IPA Server -> Configuration" add user `ldapuser1'
su - ldapuser1

# PAM
ldd `which login`
cat /etc/pam.d/login
cat /etc/pam.d/system-auth

# auth config
authconfig --test
authconfig-tui
yum install authconfig-gtk -y

# LDAP setup
# nscd - name services cache daemon
# pam_krb5 is for authconfig-gtk silence about krb auth
yum install nscd pam-ldap nss-pam-ldapd pam_krb5 -y
authconfig-gtk
# - User account database: LDAP
# - To use TLS, obtain the CA cert in PEM format from the FreeIPA WebUI:
#   Authentication -> Certificate -> 1 (CN=Certificate Authority,O=...) ->
#   -> Actions -> Download;
#   Then specify its URI in "Download CA Certificate"
# - Use Password auth

# autofs w/NFS server
# create `guests' group in LDAP
# install NFS server and autofs, if absent
yum install -y autofs
yum install -y nfs-utils
# prepare data dir we'll share via NFS
mkdir -p /data/guests/ldapuser1
chgrp -R guests /data/guests
chmod 750 /data/guests
chown ldapuser1 /data/guests/ldapuser1
chmod 700 /data/guests/ldapuser1
# set up NFS share
echo "/data/guests  -rw  *(rw,no_root_squash)" > /etc/exports
systemctl enable nfs --now  
showmount -e centos7.localdomain
# set up autofs: it'll automatically mount rw /data/guests/ldapuser1 to 
# to /mnt/ldapuser1 on the first access attempt to /mnt/ldapuser1
# and so on for /data/guests/<username> and /mnt/username
echo "/mnt   /etc/auto.guests" >> /etc/auto.master
echo "*  -rw  centos7.localdomain:/data/guests/&" > /etc/auto.guests
systemctl enable autofs --now
# then try to
su - ldapuser1
ls /mnt/ldapuser1


##
